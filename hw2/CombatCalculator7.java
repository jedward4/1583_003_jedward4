//import Scanner class
import java.util.Scanner;
//insert boilerplate code
public class CombatCalculator7{
	public static void main(String[] args){
		/* write homework #3 here
		Declare variable for user input and initialize with a new Scanner object
		*/
		//Scanner input preamble
		System.out.println("Hark brave Hero! A foul beast approaches, prepare to defend yourself.");
		System.out.println();
		//User input variable and Scanner initilization
		Scanner input = new Scanner(System.in);
		

		//Monster data variables
		//Declare variable for monster's name and initialize it to "goblin"
		String MonsterName = "goblin";
		//Declare variable for monster's health and initialize it to 100
		int MonsterHealth = 100;
		//declare variable for monster's attack power and initialize it to 15
		int MonsterAP = 15;

		//Hero data variables
		//Declare variable for Hero's health and initialize it to 100
		int HeroHP = 100;
		//Declare variable for Hero's attack power and initialize it to
		int HeroAP = 12;
		//Declare variable for Hero's magic power and intialize it to 0
		int HeroMP = 0;

		/*Loop Control*/
		//Declare loop control variable and initialize it to true
		boolean CombatTrue = true;

		//While the loop control variable is true
		while (CombatTrue == true){

			//Report Combat Stats
			System.out.println("The creature is revealed");
			System.out.println();
			//Print the Monster's name
			System.out.println("You are fighting a " + MonsterName + "!");
			//Print the Monster's health
			System.out.println("The monster's HP: " + MonsterHealth);

			//Print the Player's health
			System.out.println();
			System.out.println("Your HP: " + HeroHP);
			//Print the Player's magic points
			System.out.println("Your MP: " + HeroMP );
			//Combat menu prompt
			System.out.println();
			System.out.println("Combat Options:");
			//Print option 1: Sword Attack
			System.out.println("1.) Sword Attack");
			//Print option 2: Cast Spell
			System.out.println("2.) Cast Spell");
			//Print option 3: Charge Mana
			System.out.println("3.) Charge Mana");
			//Print option 4: Run Away
			System.out.println("4.) Run Away");
			//Prompt player for action
			System.out.println("What action do you want to perform?");

			//Declare variable for user input (as number) and acquire value from Scanner object
			int userinput = input.nextInt();
			//output value from Scanner
			System.out.println(userinput);
		
			switch (userinput) {
				//If player chose option 1, (check with equality operator)
				case 1: {
					/*Calculate damage & update monster health using subtraction
					Calculation: new monster health is old monster health minus hero attack power
					*/
					MonsterHealth = MonsterHealth - HeroAP;
					/*print attack text:
					 "You strike the <mosnter name> with your sword for <hero attack> damage"
					 */
					System.out.println("You strike the " + MonsterName + " with your sword for " + HeroAP + " damage");
					System.out.println();
					//Print the Monster's name
					System.out.println("You are fighting a " + MonsterName + "!");
					//Print the Monster's health
					System.out.println("The monster's HP: " + MonsterHealth);

					//Print the Player's health
					System.out.println();
					System.out.println("Your HP: " + HeroHP);
					//Print the Player's magic points
					System.out.println("Your MP: " + HeroMP);
					/*If monster's health is 0 or below
					Stop combat loop my settign control variable to false
					Print victory message: "You defeated the <monster name>!"
					*/
					if (MonsterHealth <= 0){
						CombatTrue = !CombatTrue;
						System.out.println("You defeated the " + MonsterName + " !");
					}
					break;
				}

				//Else if player chose option 2, (check with equality operator)
				case 2: {
					//If player has 3 or more magic points
					if(HeroMP >= 3){
					/*Increment magic points and update player magic using addition
					Calculation: new hero magic is old hero magic plus one
					*/
					MonsterHealth = MonsterHealth / 2;
					//Reduce player's mana points by the spell cost using subtraction
					//Calculation: new magic power is old magic power minus 3
					HeroMP = HeroMP - 3;
					/*print spell message:
					"you cast the weaken spell on the monster."
					*/
					System.out.println("You cast the weaken spell on the mosnter.");
					System.out.println();
					//Print the Monster's name
					System.out.println("You are fighting a " + MonsterName + "!");
					//Print the Monster's health
					System.out.println("The monster's HP: " + MonsterHealth);

					//Print the Player's health
					System.out.println();
					System.out.println("Your HP: " + HeroHP);
					//Print the Player's magic points
					System.out.println("Your MP: " + HeroMP);
					/*If monster's health is 0 or below
					Stop combat loop my settign control variable to false
					Print victory message: "You defeated the <monster name>!"
					*/
					if (MonsterHealth <= 0){
						CombatTrue = !CombatTrue;
						System.out.println("You defeated the " + MonsterName + " !");
					}
					//Else
					else{
						//print the message: "You don't have enough mana."
						System.out.println("You don't have enough mana.");
					}
				}
					break;
				}
				// Else if the player chose option 3, (check with equality operator)
				case 3: {
					/*Increment magic points and update player's magic using division
					Calculation: new monster health is old monster health divided by two
					*/
					HeroMP = HeroMP + 1;
					/*print charging message:
					"You focus and charge your magic power."
					*/
					System.out.println("You focus and charge your magic power.");
					System.out.println();
					System.out.println("You are fighting a " + MonsterName + "!");
					//Print the Monster's health
					System.out.println("The monster's HP: " + MonsterHealth);

					//Print the Player's health
					System.out.println();
					System.out.println("Your HP: " + HeroHP);
					//Print the Player's magic points
					System.out.println("Your MP: " + HeroMP);
					break;
				}
				//Else if the player chose option 4, (check with equality operator)
				case 4: {
				
					//Stop combat loop by setting control variable to false
					CombatTrue = !CombatTrue;
					/*
					print retreat message:
					"Your run away!"
					*/
					System.out.println("You run away!");
					break;
				}
				//Else the player chose incorrectly
				default: {
					/*print an error message:
					"I don't understand that command."
					*/
					System.out.println("I don't understand that command.");
				}
				
		}
		}
			


	}
}