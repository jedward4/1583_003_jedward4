//import Scanner class
import java.util.Scanner;
//insert boilerplate code
public class CombatCalculator3{
	public static void main(String[] args){
		/* write homework #3 here
		Declare variable for user input and initialize with a new Scanner object
		*/
		//Scanner input preamble
		System.out.println("Hark brave Hero! A foul beast approaches, prepare to defend yourself.");
		System.out.println();
		//User input variable and Scanner initilization
		Scanner input = new Scanner(System.in);
		

		//Monster data variables
		//Declare variable for monster's name and initialize it to "goblin"
		String MonsterName = "goblin";
		//Declare variable for monster's health and initialize it to 100
		int MonsterHealth = 100;
		//declare variable for monster's attack power and initialize it to 15
		int MonsterAP = 15;

		//Hero data variables
		//Declare variable for Hero's health and initialize it to 100
		int HeroHP = 100;
		//Declare variable for Hero's attack power and initialize it to
		int HeroAP = 12;
		//Declare variable for Hero's magic power and intialize it to 0
		int HeroMP = 0;

		//Report Combat Stats
		System.out.println("The creature is revealed");
		System.out.println();
		//Print the Monster's name
		System.out.println("You are fighting a " + MonsterName + "!");
		//Print the Monster's health
		System.out.println("The monster's HP: " + MonsterHealth);

		//Print the Player's health
		System.out.println();
		System.out.println("Your HP: " + HeroHP);
		//Print the Player's magic points
		System.out.println("Your MP: " + HeroMP);

		//Combat menu prompt
		System.out.println();
		System.out.println("Combat Options:");
		//Print option 1: Sword Attack
		System.out.println("1.) Sword Attack");
		//Print option 2: Cast Spell
		System.out.println("2.) Cast Spell");
		//Print option 3: Charge Mana
		System.out.println("3.) Charge Mana");
		//Print option 4: Run Away
		System.out.println("4.) Run Away");
		//Prompt player for action
		System.out.println("What action do you want to perform?");
		//Declare variable for user input (as number) and acquire value from Scanner object
		int userinput = input.nextInt();
		//output value from Scanner
		System.out.print(userinput);


	}
}