//insert boilerplate code
public class CombatCalculator2{
	public static void main(String[] args){
		// write homework #2 here
		//Monster data variables
		//Declare variable for monster's name and initialize it to "goblin"
		String MonsterName = "goblin";
		//Declare variable for monster's health and initialize it to 100
		int MonsterHealth = 100;
		//declare variable for monster's attack power and initialize it to 15
		int MonsterAP = 15;

		//Hero data variables
		//Declare variable for Hero's health and initialize it to 100
		int HeroHP = 100;
		//Declare variable for Hero's attack power and initialize it to
		int HeroAP = 12;
		//Declare variable for Hero's magic power and intialize it to 0
		int HeroMP = 0;

		//Report Combat Stats
		//Print the Monster's name
		System.out.println("You are fighting a " + MonsterName + "!");
		//Print the Monster's health
		System.out.println("The monster's HP: " + MonsterHealth);
		//Print the Player's health
		System.out.println("Your HP: " + HeroHP);
		//Print the Player's magic points
		System.out.println("Your MP: " + HeroMP);
	}
}